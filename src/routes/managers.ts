import express from "express";
import { config } from "../config";
import * as dataApi from "../dataApi";
import { validatePagination } from "../utils/pagination";

const managersRoute: express.Router = express.Router();

managersRoute.get("/", (req: express.Request, res: express.Response) => {
  try {
    const pagination = validatePagination(req.query);
    let managers;

    const searchTerm = req.query.searchTerm;
    if (searchTerm) {
      if (searchTerm.length < config.minSearchTermLength) {
        res.status(400).send(`search did not meet the minimum requirement of ${config.minSearchTermLength}`);
      } else {
        managers = dataApi.search("managers",
          ["attributes.name", "relationships.account.data.attributes.email"],
           searchTerm);
      }
    } else {
      managers = dataApi.get("managers", pagination.page, pagination.limit );
    }

    res.status(200).send({data: managers});
  } catch (error) {
    res.status(403).send("Error getting managers: " + error.message);
  }
});

managersRoute.post("/", (req: express.Request, res: express.Response) => {
  res.status(501).send();
});

managersRoute.put("/", (req: express.Request, res: express.Response) => {
  res.status(501).send();
});

managersRoute.delete("/", (req: express.Request, res: express.Response) => {
  res.status(501).send();
});

export { managersRoute };
