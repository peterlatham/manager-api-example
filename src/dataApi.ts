import { db } from "./data";
import { searchEntries } from "./utils/search";

const get = (table: string, page: number, limit: number): any[] => {
  const startIndex = (page - 1) * limit;
  const endIndex = (startIndex + limit);
  const results: any[] = db[`${table}`].slice(startIndex, endIndex);
  return results;
};

const search = (table: string, keys: string[], term: string): any[] => {
  return searchEntries(term, keys, db[`${table}`]);
};

export { get, search };
