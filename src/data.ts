import fs from "fs";
import { config } from "./config";

const employees = JSON.parse(fs.readFileSync(config.dataSource).toString());

let managers = employees.data.filter( (employee: { type: string; }) => {
  if (employee.type === "employees") {
    return employee;
  }
});

// it seems some employees are in the included attribute
const included = employees.included.filter( (employee: { type: string; }) => {
  if (employee.type === "employees") {
    return employee;
  }
});

managers = managers.concat(included);

const accounts = employees.included.filter( (account: { type: string; }) => {
  if (account.type === "accounts") {
    return account;
  }
});

managers = managers.map( (mgr: { relationships: { account: { data: { id: string; }; }; }; }) => {
  const acctId = mgr.relationships.account.data.id;
  const acct = accounts.find( (account: { id: string; }) => {
    if (account.id === acctId) {
      return account;
    }
  });
  if (acct) {
    mgr.relationships.account.data = acct;
  }
  return mgr;
});

const db: any = {
  managers,
};

export { db };
