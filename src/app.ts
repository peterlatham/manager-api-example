import express from "express";
import { config } from "./config";
import { managersRoute } from "./routes/managers";

const app: express.Application = express();

app.use("/v1/api/managers", managersRoute);

app.listen(
  config.port,
  () => console.info(`Server ready`),
);

export { app };
