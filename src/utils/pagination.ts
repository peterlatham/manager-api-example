import { config } from "../config";

const validatePagination = ( query: any): any => {
  let page: number = +query.page;
  if (isNaN(page) || page < 1) {
    page = 1;
  }

  let limit: number = +query.limit;
  if (isNaN(limit)) {
    limit = config.defaultLimit;
  } else if (limit > config.maxLimit) {
    limit = config.maxLimit;
  } else if (limit < 1) {
    limit = 1;
  }

  return {page, limit};
};

export { validatePagination };
