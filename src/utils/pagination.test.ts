import chai from "chai";
import * as mocha from "mocha";
import { config } from "../config";
import * as pagination from "./pagination";

const expect = chai.expect;

describe("Pagination validation unit tests", function() {

  it("Should return the defaultLimit when no limit is specified", function() {
    const query = { page: "1"};
    const result = pagination.validatePagination(query);
    expect(result).to.haveOwnProperty("limit");
    expect(result.limit).to.be.a("number");
    expect(result.limit).to.eq(config.defaultLimit);
  });
  it("Should return the maxLimit when more than the max limit is specified", function() {
    const query = { page: "1", limit: "1000"};
    const result = pagination.validatePagination(query);
    expect(result).to.haveOwnProperty("limit");
    expect(result.limit).to.be.a("number");
    expect(result.limit).to.eq(config.maxLimit);
  });
  it("Should return a limit of 1 when less than 1 is specified", function() {
    const query = { page: "1", limit: "0"};
    const result = pagination.validatePagination(query);
    expect(result).to.haveOwnProperty("limit");
    expect(result.limit).to.be.a("number");
    expect(result.limit).to.eq(1);
  });

  it("Should return a page of 1 when invalid input is specified", function() {
    const query = { page: "joffery", limit: "10"};
    const result = pagination.validatePagination(query);
    expect(result).to.haveOwnProperty("page");
    expect(result.page).to.be.a("number");
    expect(result.page).to.eq(1);
  });

  it("Should return a page of 1 when less than 1 is specified", function() {
    const query = { page: "0", limit: "12"};
    const result = pagination.validatePagination(query);
    expect(result).to.haveOwnProperty("page");
    expect(result.page).to.be.a("number");
    expect(result.page).to.eq(1);
  });
});
