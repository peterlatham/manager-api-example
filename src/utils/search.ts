import FuzzySearch from "fuzzy-search";

const searchEntries = (term: string, keys: string[], entries: any[]): any[] => {
  const searcher = new FuzzySearch(entries, keys, {
    caseSensitive: false, sort: true,
  });

  const result = searcher.search(term);
  return result;
};

export { searchEntries };
