import chai from "chai";
import chaiHttp = require("chai-http");
import * as mocha from "mocha";
import { app } from "./app";
import { config } from "./config";

chai.use(chaiHttp);
const expect = chai.expect;

describe("Manager API example tests", function() {
  before("Initilization for tests", () => {
    // set up any tests here
  });

  describe("General server sanity checks", async function() {
    it("Should return a 200 on /v1/api/manager endpoint", async function() {
      try {
        const res = await chai.request(app)
          .get("/v1/api/managers");
        expect(res).to.have.status(200);
      } catch (error) {
        throw error;
      }
    });

    it("Should return a 404 on an invalid endpoint", async function() {
      try {
        const res = await chai.request(app)
          .get("/v1/api/employees");
        expect(res).to.have.status(404);
      } catch (error) {
        throw error;
      }
    });
  });

  describe("Test the pagination on the managers endpoint ", async function() {
    it("Should return default limit of managers", async function() {
      try {
        const res = await chai.request(app)
          .get("/v1/api/managers");
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.haveOwnProperty("data");
        expect(res.body.data).to.be.an("array");
        expect(res.body.data.length).to.eq(config.defaultLimit);
      } catch (error) {
        throw error;
      }
    });

    it("Should return all managers (11) with the max limit requested", async function() {
      try {
        const res = await chai.request(app)
          .get("/v1/api/managers")
          .query({page: "1", limit: "100"});
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.haveOwnProperty("data");
        expect(res.body.data).to.be.an("array");
        expect(res.body.data.length).to.eq(11);
      } catch (error) {
        throw error;
      }
    });

    it("Should return all managers that have type employee", async function() {
      try {
        const res = await chai.request(app)
          .get("/v1/api/managers")
          .query({page: "1", limit: "25"});
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.haveOwnProperty("data");
        expect(res.body.data).to.be.an("array");
        expect(res.body.data.length).to.eq(11);
        const employeeElements = res.body.data.filter((element: { type: string; }) => {
          if (element.type === "employees") {
            return element;
          }
        });
        expect(employeeElements.length).to.be.eq(11);
      } catch (error) {
        throw error;
      }
    });

    it("Should return employee 142 when request page 3, limit 1", async function() {
      try {
        const res = await chai.request(app)
          .get("/v1/api/managers")
          .query({page: "3", limit: "1"});
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.haveOwnProperty("data");
        expect(res.body.data).to.be.an("array");
        expect(res.body.data.length).to.eq(1);
        expect(res.body.data[0]).to.haveOwnProperty("type");
        expect(res.body.data[0].type).to.be.eq("employees");
        expect(res.body.data[0]).to.haveOwnProperty("id");
        expect(res.body.data[0].id).to.be.eq("142");
      } catch (error) {
        throw error;
      }
    });
  });

  describe("Test the search completion on the managers endpoint  ", async function() {
    it("Should return 4 matches for search term 'har' ", async function() {
      try {
        const res = await chai.request(app)
          .get("/v1/api/managers")
          .query({searchTerm: "har"});
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.haveOwnProperty("data");
        expect(res.body.data).to.be.an("array");
        expect(res.body.data.length).to.eq(4);
        expect(res.body.data[0]).to.deep.include({ id: "323"});
        expect(res.body.data[1]).to.deep.include({ id: "139"});
        expect(res.body.data[2]).to.deep.include({ id: "139"});
        expect(res.body.data[3]).to.deep.include({ id: "142"});
      } catch (error) {
        throw error;
      }
    });

    it("Should return no matches on search term 'po' ", async function() {
      try {
        const res = await chai.request(app)
          .get("/v1/api/managers")
          .query({searchTerm: "po"});
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.haveOwnProperty("data");
        expect(res.body.data).to.be.an("array");
        expect(res.body.data.length).to.eq(0);
      } catch (error) {
        throw error;
      }
    });

    it("Should return only one match for term 'cum' ", async function() {
      try {
        const res = await chai.request(app)
          .get("/v1/api/managers")
          .query({searchTerm: "cum"});
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.haveOwnProperty("data");
        expect(res.body.data).to.be.an("array");
        expect(res.body.data.length).to.eq(1);
        expect(res.body.data[0]).to.deep.include({ id: "201"});
      } catch (error) {
        throw error;
      }
    });

    it("Should ignore the page and limit params and return 4 from search", async function() {
      try {
        const res = await chai.request(app)
          .get("/v1/api/managers")
          .query({page: "1", limit: "1", searchTerm: "har"});
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.haveOwnProperty("data");
        expect(res.body.data).to.be.an("array");
        expect(res.body.data.length).to.eq(4);
        expect(res.body.data[0]).to.deep.include({ id: "323"});
        expect(res.body.data[1]).to.deep.include({ id: "139"});
        expect(res.body.data[2]).to.deep.include({ id: "139"});
        expect(res.body.data[3]).to.deep.include({ id: "142"});
      } catch (error) {
        throw error;
      }
    });
  });
});
