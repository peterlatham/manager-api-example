const config = {
  dataSource: "data/employees.json",
  defaultLimit: 5,
  maxLimit: 100,
  minSearchTermLength: 1,
  port: 4001,
};

export { config };
