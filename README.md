# manager api example

A simple API with a single endpoint at ```/v1/api/managers```
 

## About 
This was to demonstrate a simple endpoint to retrieve information from a dataset that contains manager information. It should support simple pagination and search queries. 

## Getting Started 
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites
- [NodeJS 10.16.0](https://nodejs.org/en/)


### Installing
Use npm to install the dependencies and build the project

```
npm install
npm run build
```

The server can be started running npm script 'start

```
npm run start
```

This will start a local manager-api-example service on http://localhost:4001/v1/api/managers

e.g. 

http://localhost:4001/v1/api/managers?page=2&limit=5

## Running the tests
You can run the Mocha test suite by running the npm "test" script

```
npm run test
```

##  Usage 
### Query parameters. 

**[page]** *(type: `String`, default: `1`)*

Page of results being returned. 

---

**[limit]** *(type: `String`, default: `5`, max: `100`)*

Number to limit results returned per page.

---

**[searchTerm]** *(type: `String`)*

Term to return managers that have a matching name / email in order of best match - ignores the pagination limits. 

---
### Server Configuration 



```
const config = {
  dataSource: "data/employees.json",
  defaultLimit: 5,
  maxLimit: 100,
  minSearchTermLength: 1,
  port: 4001,
};
```

**[dataSource]** *(type: `String`, default: `data/employees.json`)*

Location to load the data from. 

---

**[defaultLimit]** *(type: `Number`, default: `5`,)*

Number that query limit will default to if not specified. 

---

**[maxLimit]** *(type: `Number`, default: `100`)*

Max number that query limit can be set to. 

---

**[minSearchTermLength]** *(type: `Number`, default: `1`)*

Minimum length that search query can be, before it returns any results. 

---

**[port]** *(type: `Number`, default: `4001`)*

Port number to run the service on.

## Enhancements
- Subset of the manager object
  - currently full manager object -combined with account information is returned. If providing information only for autocomplete, then objects containing only fields for email, name and id can be returned 
  
  ```
  { 
    data: [
    { 
      id: "42", 
      name": Harriet Henrison, 
      "email": h.h@henrison.com
    }]
  }
  ```
  
--- 

- Returning pagination links in the response e.g. Next, Previous. 

--- 

- Better typing on data structures - depending on whether the data is sourced from a SQL or NoSQL store. 

---

- Limit on returned search results. 

---

- Config could be passed into server before start to load different configuraitons for test.  

---

- Performance on retrieving data, searching and retrieving data. Data is stored and 'processed' in Javascript arrays. As the set is not large, it could be a lot more performant given how the data is stored. 


## Built Using 
- [Express](https://expressjs.com/) - Server Framework
- [NodeJs](https://nodejs.org/en/) - Server Environment
- [Typescript](https://www.typescriptlang.org) - Typed superset of Javascript
- [Mocha](https://mochajs.org) - JavaScript test framework
